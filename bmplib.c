#include "bmplib.h"

BMP* readBMPFile(char* filePath)
{

    FILE* fp = fopen(filePath, "rb");
    if(fp == NULL)
    {
        perror("Error reading the file");
        return NULL;
    }

    BMP *bmp;
    bmp = malloc(sizeof(BMP));


    bmp->bmpHeader = malloc(sizeof (BMPHeader));
    bmp->bmpInfo = malloc(sizeof (BMPInfoHeader));

    fread(&bmp->bmpHeader->signature, 1, 2, fp);
    if(bmp->bmpHeader->signature != 0x4d42)
    {

        printf("Not a BMP file!\n");
        return NULL;
    }

    fread(&(bmp->bmpHeader->FileSize), sizeof(bmp->bmpHeader->FileSize), 1, fp);
    fread(&(bmp->bmpHeader->reserved), sizeof(bmp->bmpHeader->reserved), 1, fp);
    fread(&(bmp->bmpHeader->DataOffset), sizeof(bmp->bmpHeader->DataOffset), 1, fp);

    fread(bmp->bmpInfo, sizeof(BMPInfoHeader), 1, fp);

    bmp->ColorDataSize = bmp->bmpHeader->DataOffset - ftell(fp);

    bmp->ColorData = malloc(bmp->ColorDataSize);

    float multiplier = bmp->bmpInfo->BitCount / 8.0;



    int width = (int) ceil((bmp->bmpInfo->Width * multiplier));
    unsigned char pading = (4 - width % 4) % 4;
    width += pading;
    int actualwidth = bmp->bmpInfo->Width;

    bmp->bmpInfo->Width = width / multiplier;



    unsigned int BufferSize = width * bmp->bmpInfo->Height;

    unsigned char buffer[BufferSize];

    bmp->PixelData = malloc(bmp->bmpInfo->Width * bmp->bmpInfo->Height * sizeof(PixelData));

    fread(bmp->ColorData, 1, bmp->ColorDataSize, fp);

    fread(buffer, BufferSize, 1, fp);

    int index = 0;
    if(bmp->bmpInfo->BitCount < 16)
    {



        for(int i = 0; i < BufferSize; i++)
        {



            for(int j = 0; j < GetPixelsPerByte(bmp); j++)
            {
                for(int k = 0; k < bmp->bmpInfo->BitCount; k++)
                {

                    bmp->PixelData[index].index += (buffer[i] & (1 << k + j * bmp->bmpInfo->BitCount)) >> j * bmp->bmpInfo->BitCount;

                }

                index++;

            }
        }

        if(actualwidth != bmp->bmpInfo->Width)
            CropImage(bmp, 0, 0, actualwidth, bmp->bmpInfo->Height);

    } else {

        for(int i = 0; i < bmp->bmpInfo->Width * bmp->bmpInfo->Height;i++)
        {



                unsigned short combined = (buffer[index+1] << 8) + buffer[index];

                index+=2;

                for(int j = 0; j < 5; j++)
                    bmp->PixelData[i].b += combined & (1 << j);

                for(int j = 0; j < 6; j++)
                    bmp->PixelData[i].g += combined & (1 << j+5);
                bmp->PixelData[i].g = bmp->PixelData[i].g >> 5;

                for(int j = 0; j < 5; j++)
                    bmp->PixelData[i].r += combined & (1 << j+6+5);
                bmp->PixelData[i].r = bmp->PixelData[i].r >> 5+6;





        }


    }




    fclose(fp);

    return bmp;

}


int writeBMPFile(BMP *bmp, char* filePath)
{


    FILE* fp = fopen(filePath, "wb");

    if(fp == NULL)
    {
        perror("Error writing to file");
        return 1;
    }


    fwrite("", 0, 0, fp);

    fwrite(&(bmp->bmpHeader->signature), 2, 1, fp);
    fwrite(&(bmp->bmpHeader->FileSize), sizeof(bmp->bmpHeader->FileSize), 1, fp);
    fwrite(&(bmp->bmpHeader->reserved), sizeof(bmp->bmpHeader->reserved), 1, fp);
    fwrite(&(bmp->bmpHeader->DataOffset), sizeof(bmp->bmpHeader->DataOffset), 1, fp);

    fwrite(bmp->bmpInfo, sizeof(BMPInfoHeader), 1, fp);

    fwrite(bmp->ColorData, 1, bmp->ColorDataSize, fp);



    float multiplier = bmp->bmpInfo->BitCount / 8.0;

    int width = (int) ceil((bmp->bmpInfo->Width * multiplier));
    unsigned char pading = (4 - width % 4) % 4;
    width += pading;

    unsigned int BufferSize = width * bmp->bmpInfo->Height;


    unsigned char buffer[BufferSize];
    int index = 0;
    if(bmp->bmpInfo->BitCount < 16)
    {


        for(int i = 0; i < bmp->bmpInfo->Height; i++)
        {

            for(int j = 0; j < width - pading; j++)
            {
                buffer[i * width + j] = 0;



                if((j+1) % (width - pading) == 0 && bmp->bmpInfo->Width % (8 / bmp->bmpInfo->BitCount) != 0)
                {


                    for(int k = 0; k < bmp->bmpInfo->Width % (8 / bmp->bmpInfo->BitCount); k++)
                    {
                        buffer[i * width + j] += bmp->PixelData[index++].index << bmp->bmpInfo->BitCount - (k * bmp->bmpInfo->BitCount);
                    }


                    continue;
                }


                for(int k = 0; k < GetPixelsPerByte(bmp); k++)
                {
                    buffer[i * width + j] += bmp->PixelData[index++].index << (k * bmp->bmpInfo->BitCount);
                }
            }

            for(int k = 0; k < pading; k++)
            {
                buffer[i * width + width-pading + k] = 0;
            }


        }


    }
    else
    {




        for(int i = 0; i < bmp->bmpInfo->Width * bmp->bmpInfo->Height;i++)
        {

                unsigned short combined = (bmp->PixelData[i].r << 5+6) + (bmp->PixelData[i].g << 5) + bmp->PixelData[i].b;


                int buf = 0;
                int buf1 = 0;

                for(int j = 0; j < 8; j++)
                    buf += combined & (1 << j);

                for(int j = 8; j < 16; j++)
                    buf1 += combined & (1 << j);
                buf1 = buf1 >> 8;

                buffer[index+1] = buf1;
                buffer[index] = buf;
                index+=2;

                if(index % width == 0)
                {

                    for(int k = 0; k < pading; k++)
                    {
                        buffer[index++] = 0;
                    }

                }

        }

    }


    fwrite(buffer, BufferSize, 1, fp);


    fclose(fp);
    return 0;

}



void freeBMP(BMP *bmp)
{

    free(bmp->bmpHeader);
    free(bmp->bmpInfo);
    free(bmp->ColorData);
    free(bmp->PixelData);
    free(bmp);

}


int GetPixelsPerByte(BMP *bmp)
{

    int result = 0;

    if(bmp->bmpInfo->BitCount < 16)
        result = 8 / bmp->bmpInfo->BitCount;
    else
        result = bmp->bmpInfo->BitCount / 8;


    return result;

}


int GetBufferSize(BMP *bmp)
{

    return (int)(ceil(bmp->bmpInfo->BitCount / 8.0 * bmp->bmpInfo->Width) * bmp->bmpInfo->Height);

}
