#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "bmplib.h"
#include "filter.h"
#include "main.h"


int main(int argc, char *argv[])
{




    char fileIn[1024] = "";
    char fileOut[1024] = "";


    // Handle command line arguments for file path.
    if(argc >= 2)
        memcpy(fileIn, argv[1], strlen(argv[1])+1);

    if(argc >= 3)
        memcpy(fileOut, argv[2], strlen(argv[2])+1);


    // Ask the user for a path if not provided through CLI
    if(strlen(fileIn) == 0)
    {
        printf("Enter input file path: ");
        scanf("%s", fileIn);
    }

    if(strlen(fileOut) == 0)
    {
        printf("Enter output file path: ");
        scanf("%s", fileOut);
    }


    BMP *bmp = readBMPFile(fileIn);
    if(bmp == NULL)
    {
        return 1;
    }

    print_info(bmp);


    // If there are 4 or more arguments that means there's a command list.
    if(argc >= 4)
    {

        for(int i = 3; i < argc; i++)
        {

            if(strcmp("blur", argv[i]) == 0)
            {

                BlurImage(bmp);

            } else if (strcmp("flip", argv[i]) == 0)
            {

                FlipImage(bmp);


            } else if (strcmp("crop", argv[i]) == 0)
            {



                if(argc < i + 4)
                {

                    printf("You need to supply proper parameters fro the crop!");

                }
                else
                {

                    unsigned int x1 = atoi(argv[i+1]);
                    unsigned int y1 = atoi(argv[i+2]);
                    unsigned int x2 = atoi(argv[i+3]);
                    unsigned int y2 = atoi(argv[i+4]);

                    CropImage(bmp, x1, y1, x2, y2);


                    i += 4;


                    //CropImage()
                }


            } else
            {

                printf("Invalid command: %s", argv[i]);

            }

        }



    } else // If there aren't then ask the user for the commands
    {



        printf("Select the command by entering the number corresponding to it:\n");
        printf("1. Flip image:\n");
        printf("2. Blur image:\n");
        printf("3. Crop image:\n");
        printf("4. Save and quit:\n");


        unsigned char loopActive = 1;
        while(loopActive)
        {



            int choice;

            scanf("%d", &choice);

            switch(choice)
            {
                case 1:
                    FlipImage(bmp);
                    break;

                case 2:
                    BlurImage(bmp);
                    break;

                case 3:
                {

                    unsigned int x1;
                    unsigned int y1;

                    unsigned int x2;
                    unsigned int y2;

                    printf("Enter the bottom left X coordinate:");
                    scanf("%d", &x1);

                    printf("Enter the bottom left Y coordinate:");
                    scanf("%d", &y1);

                    printf("Enter the top right X coordinate:");
                    scanf("%d", &x2);


                    printf("Enter the top right Y coordinate:");
                    scanf("%d", &y2);


                    CropImage(bmp, x1, y1, x2, y2);
                    break;
                }

                case 4:
                    loopActive = 0;
                    break;
                default:
                printf("Invalid command!");
                break;

            }


        }


    }

    print_info(bmp);



    if(writeBMPFile(bmp, fileOut) == 1)
    {
        return 1;
    }


    return 0;
}




void print_info(BMP *bmp)
{

    printf("Signature: %x\n", bmp->bmpHeader->signature);
    printf("File Size: %d\n", bmp->bmpHeader->FileSize);
    printf("Reserved: %d\n", bmp->bmpHeader->reserved);
    printf("Data Offset: %x\n", bmp->bmpHeader->DataOffset);



    printf("Header Size: %d\n", bmp->bmpInfo->Size);
    printf("Width: %d\n", bmp->bmpInfo->Width);
    printf("Height: %d\n", bmp->bmpInfo->Height);
    printf("Planes: %d\n", bmp->bmpInfo->Planes);
    printf("Bit Count: %d\n", bmp->bmpInfo->BitCount);
    printf("Compression: %d\n", bmp->bmpInfo->Compression);
    printf("Image Size: %d\n", bmp->bmpInfo->ImageSize);
    printf("X Pixels Per Meter: %d\n", bmp->bmpInfo->XPixelsPM);
    printf("Y Pixels Per Meter: %d\n", bmp->bmpInfo->YPixelsPM);
    printf("Colors Used: %d\n", bmp->bmpInfo->ColorsUsed);
    printf("Colors Important: %d\n", bmp->bmpInfo->ColorsImportant);

}
