#ifndef FILTER_H_INCLUDED
#define FILTER_H_INCLUDED
#include "bmplib.h"

//-----------------------
//DESCRIPTION: The function takes a BMP picture and flips it vertically.
//
//PARAMETERS: *bmp, pointer to a BMP structure. The function directly modifies it.
//
//RETURN VALUE: N/A
//-----------------------
void FlipImage(BMP *bmp);


//-----------------------
//DESCRIPTION: The function takes a BMP pciture and blurs it using a box blur.
//
//PARAMETERS: *bmp, pointer to a BMP structure. The function directly modifies it.
//
//RETURN VALUE: N/A
//-----------------------
void BlurImage(BMP *bmp);


//-----------------------
//DESCRIPTION: The function takes a BMP picture and crops it.
//
//PARAMETERS: *bmp, pointer to a BMP structure, The function directly modifies it.
// unsigned int x1 - Bottom left X coordinate of the crop area.
// unsigned int y1 - Bottom left Y coordinate of the crop area.
// unsigned int x2 - Top right X coordinate of the crop area.
// unsigned int y2 - Top right X coordinate of the crop area.
//
//RETURN VALUE: N/A
//-----------------------
void CropImage(BMP *bmp, unsigned int x1, unsigned y1, unsigned int x2, unsigned int y2);

#endif // FILTER_H_INCLUDED
