#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "math.h"

typedef struct BMPHeader
{
    unsigned short signature;
    unsigned int FileSize;
    unsigned int reserved;
    unsigned int DataOffset;

} BMPHeader;

typedef struct BMPInfoHeader
{

    int Size;
    int Width;
    int Height;
    int Planes : 16;
    int BitCount : 16;
    int Compression;
    int ImageSize;
    int XPixelsPM;
    int YPixelsPM;
    int ColorsUsed;
    int ColorsImportant;

} BMPInfoHeader;


typedef struct PixelData
{

    unsigned char index;
    unsigned short r;
    unsigned short g;
    unsigned short b;

} PixelData;


typedef struct BMP
{

    BMPHeader *bmpHeader;
    BMPInfoHeader *bmpInfo;
    unsigned char *ColorData;
    int ColorDataSize;
    PixelData *PixelData;



} BMP;




//-----------------------
//DESCRIPTION: The function takes a file path, reads the BMP picture and converts it to a BMP struct.
//
//PARAMETERS: char* filepath, a character array, holding the file path.
//
//RETURN VALUE: BMP*, the newly created BMP struct
//-----------------------
BMP* readBMPFile(char* filePath);

//-----------------------
//DESCRIPTION: The function takes a pointer to a BMP picture and a file path and writes the image to file.
//
//PARAMETERS: *bmp, pointer to a BMP structure
// char* filepath, a char array, holding the file path
//
//RETURN VALUE: Returns an integer, if 1 the writing has failed, if two it has succeeded
//-----------------------
int writeBMPFile(BMP* bmp, char* filePath);

//-----------------------
//DESCRIPTION: The function takes a pointer to a BMP picture and frees the memory, including all of the structs that have been dynamically allocated.
//
//PARAMETERS: *bmp, pointer to a BMP structure
//
//RETURN VALUE: N/A
//-----------------------
void freeBMP(BMP* bmp);

//-----------------------
//DESCRIPTION: The function takes a pointer to a BMP structure and allocates Pixel Data structures in according to the height, width and bit count.
//
//PARAMETERS: *bmp, pointer to a BMP structure
//
//RETURN VALUE: The newly created Pixel Data structure
//-----------------------
unsigned short* allocPixelData(BMP* bmp);


//-----------------------
//DESCRIPTION: The function takes a pointer to a BMP structure and returns the pixels per byte.
//
//PARAMETERS: *bmp, pointer to a BMP structure
//
//RETURN VALUE: The pixels per byte
//-----------------------
int GetPixelsPerByte(BMP *bmp);
