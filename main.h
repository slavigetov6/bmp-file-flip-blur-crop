#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

//-----------------------
//DESCRIPTION: The function takes a pointer to a BMP structure and prints out all of the data.
//
//PARAMETERS: *bmp, pointer to a BMP structure
//
//RETURN VALUE: The pixels per byte
//-----------------------

void print_info(BMP *bmp);

#endif // MAIN_H_INCLUDED
