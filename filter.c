#include "filter.h"

void FlipImage(BMP *bmp)
{


    PixelData *InvertedPixelData;
    InvertedPixelData = malloc(bmp->bmpInfo->Width * bmp->bmpInfo->Height * sizeof(PixelData));

    for(int i = 0; i < bmp->bmpInfo->Width * bmp->bmpInfo->Height; i++)
    {



        int x = i % bmp->bmpInfo->Width;
        int y = (i / bmp->bmpInfo->Width);

        y = (bmp->bmpInfo->Height-1) - y;




        InvertedPixelData[i] = bmp->PixelData[y * bmp->bmpInfo->Width + x];

    }



    free(bmp->PixelData);
    bmp->PixelData = InvertedPixelData;




}


void BlurImage(BMP *bmp)
{


    if(bmp->bmpInfo->BitCount < 16)
    {

        printf("Can't blur pictures less than 16bpp!");
        return;

    }

    PixelData *InvertedPixelData;
    InvertedPixelData = malloc(bmp->bmpInfo->Width * bmp->bmpInfo->Height * sizeof(PixelData));



    for(int i = 0; i < bmp->bmpInfo->Width * bmp->bmpInfo->Height; i++)
    {

        int x = i % bmp->bmpInfo->Width;
        int y = (i / bmp->bmpInfo->Width);


        int kernelSize = 13;

        int (*sides)[2] = calloc(kernelSize * kernelSize, sizeof *sides);
        double *kernel = calloc(kernelSize * kernelSize, sizeof *kernel);

        for(int i = 0; i < kernelSize * kernelSize; i++)
        {

            int xOffset = i % kernelSize - kernelSize / 2;
            int yOffset = i / kernelSize - kernelSize / 2;

            //kernel[i] = (double)1.0/(2.0*3.14*sigma*sigma) * exp(-(abs(xOffset)* abs(xOffset) + abs(yOffset) * abs(yOffset)) / (2*sigma*sigma));
            kernel[i] = 1;


            sides[i][0] = x + xOffset;
            sides[i][1] = y + yOffset;
        }


        double kernelAverage = 0;

        unsigned int averageRed = 0;
        unsigned int averageBlue = 0;
        unsigned int averageGreen = 0;

        PixelData side;

        for(int j = 0; j < kernelSize * kernelSize; j++)
        {

            int xOut = 0;
            int yOut = 0;


            xOut = sides[j][0] < 0 || sides[j][0] >=  bmp->bmpInfo->Width;
            yOut = sides[j][1] < 0 || sides[j][1] >=  bmp->bmpInfo->Height;

            if(xOut || yOut)
            {

                averageRed += 0;
                averageGreen += 0;
                averageBlue += 0;

            } else{

                side = bmp->PixelData[sides[j][1] *  bmp->bmpInfo->Width + sides[j][0]];

                averageRed += kernel[j] * side.r;
                averageGreen += kernel[j] * side.g;
                averageBlue += kernel[j] * side.b;

            }



            kernelAverage += kernel[j];



        }

        averageRed /= kernelAverage;
        averageGreen /= kernelAverage;
        averageBlue /= kernelAverage;


        InvertedPixelData[i].r = averageRed;
        InvertedPixelData[i].g = averageGreen;
        InvertedPixelData[i].b = averageBlue;


    }


    free(bmp->PixelData);
    bmp->PixelData = InvertedPixelData;

}


void CropImage(BMP *bmp, unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
{

    unsigned int newHeight = y2 - y1;
    unsigned int newWidth = x2 - x1;

    unsigned int start_y = y1;
    unsigned int end_y = y2;

    unsigned int start_x = x1;
    unsigned int end_x = x2;


    PixelData *InvertedPixelData;
    InvertedPixelData = malloc(bmp->bmpInfo->Width * bmp->bmpInfo->Height * sizeof(PixelData));


    int index = 0;

    for(int i = start_y; i < end_y; i++)
    {

        for(int j = start_x; j < end_x; j++)
        {
            InvertedPixelData[index++] = bmp->PixelData[i * bmp->bmpInfo->Width + j];
        }
    }

    bmp->bmpInfo->Width = newWidth;
    bmp->bmpInfo->Height = newHeight;

    unsigned int headerSize = bmp->bmpInfo->Size + 14;
    bmp->bmpInfo->ImageSize = newWidth * newHeight;
    bmp->bmpHeader->FileSize = headerSize + bmp->bmpInfo->ImageSize;

    free(bmp->PixelData);
    bmp->PixelData = InvertedPixelData;

}
